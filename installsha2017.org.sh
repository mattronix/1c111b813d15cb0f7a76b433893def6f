#! /bin/bash

mkdir sha2017.org
cd sha2017.org
curl -O https://bolt.cm/distribution/bolt-latest.tar.gz
tar -xzf bolt-latest.tar.gz --strip-components=1
php app/nut init
git clone https://github.com/sha2017/sha2017.org public/theme/sha2017.org
sed -ie 's/base-2016/sha2017.org/g' app/config/config.yml
echo "Run'cd sha2017.org' and then run 'php app/nut server:run' to start the development web server."